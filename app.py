from flask import Flask, render_template, request
import requests

app = Flask(__name__)

API_KEY = "03cd08afabmsh38ca76f04dcc7cbp1b0b9bjsn76ae773f5b30"
API_URL = "https://omgvamp-hearthstone-v1.p.rapidapi.com"

headers = {
    "X-RapidAPI-Host": "omgvamp-hearthstone-v1.p.rapidapi.com",
    "X-RapidAPI-Key": API_KEY,
}


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/search", methods=["POST"])
def search():
    search_term = request.form.get("search_term")
    card_class = request.form.get("card_class")
    card_race = request.form.get("card_race")
    card_set = request.form.get("card_set")
    card_quality = request.form.get("card_quality")
    card_faction = request.form.get("card_faction")
    card_type = request.form.get("card_type")

    response = None
    unique_cards = []
    if search_term:
        response_search = requests.get(f"{API_URL}/cards/search/{search_term}", headers=headers)
        if response_search.status_code == 200:
            cards_search = response_search.json()
            unique_cards = cards_search

    if card_class:
        response_class = requests.get(f"{API_URL}/cards/classes/{card_class}", headers=headers)
        if response_class.status_code == 200:
            cards_class = response_class.json()
            if len(unique_cards) == 0:
                unique_cards = cards_class
            else:
                unique_cards = [card for card in cards_class if card in unique_cards]

    if card_race:
        response_race = requests.get(f"{API_URL}/cards/races/{card_race}", headers=headers)
        if response_race.status_code == 200:
            cards_race = response_race.json()
            if len(unique_cards) == 0:
                unique_cards = cards_race
            else:
                unique_cards = [card for card in cards_race if card in unique_cards]
    cards_with_image = 0

    if card_set:
        response_set = requests.get(f"{API_URL}/cards/sets/{card_set}", headers=headers)
        if response_set.status_code == 200:
            cards_set = response_set.json()
            if len(unique_cards) == 0:
                unique_cards = cards_set
            else:
                unique_cards = [card for card in cards_set if card in unique_cards]
    if card_quality:
        response_quality = requests.get(f"{API_URL}/cards/qualities/{card_quality}", headers=headers)
        if response_quality.status_code == 200:
            cards_quality = response_quality.json()
            if len(unique_cards) == 0:
                unique_cards = cards_quality
            else:
                unique_cards = [card for card in cards_quality if card in unique_cards]
    if card_faction:
        response_faction = requests.get(f"{API_URL}/cards/factions/{card_faction}", headers=headers)
        if response_faction.status_code == 200:
            cards_faction = response_faction.json()
            if len(unique_cards) == 0:
                unique_cards = cards_faction
            else:
                unique_cards = [card for card in cards_faction if card in unique_cards]
    if card_type:
        response_type = requests.get(f"{API_URL}/cards/types/{card_type}", headers=headers)
        if response_type.status_code == 200:
            cards_type = response_type.json()
            if len(unique_cards) == 0:
                unique_cards = cards_type
            else:
                unique_cards = [card for card in cards_type if card in unique_cards]
    for card in unique_cards:
        if 'img' in card and card['img']:
            cards_with_image += 1
    return render_template("result.html", cards=unique_cards, cards_with_image=cards_with_image)



if __name__ == "__main__":
    app.run(debug=True)
